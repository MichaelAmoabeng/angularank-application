# AngulaRank-application

This is an application that ranks GitHub Angular repository contributors.
The application was built with Backbone.js, Underscore.js, GitHub Developers API and styled with Twitter Bootstrap.
# Features
The features of the application include:
1. Repository details view.
2. Contributors' data.
3. Filter to raank the contibutors by the number of contributions, number of followers and the number of repositories published.